package com.revature.util;

public final class FinalUtil {
	public static final String HTTP_Get = "GET";
	
	
	public static final int USERNAME_TAKEN_CODE = 322;
	public static final String USERNAME_TAKEN = "USERNAME TAKEN";
	
	public static final int USERNAME_AVAILABLE_CODE = 1337;
	public static final String USERNAME_AVAILABLE = "USERNAME AVAILABLE";
	
	public static final int NOT_IMPLEMENTED_CODE = 666;
	public static final String NOT_IMPLEMENTED = "NOT IMPLEMENTED";

}
