package com.revature.dao;

import java.util.List;

import com.revature.model.Reimbursment;

public interface ReimbursmentDao {
	public boolean insertProcedure(Reimbursment reimbursment);
	public Reimbursment select(Reimbursment reimbursment);
	public List<Reimbursment> selectAll();
	public List<Reimbursment> getStaffReimbursment(Reimbursment reimbursment);
	public List<Reimbursment> getResolvedReimbursment(Reimbursment reimbursment);
	public List<Reimbursment> getPendingReimbursment(Reimbursment reimbursment);
	public List<Reimbursment> getAllResolvedReimbursment();
	public List<Reimbursment> getAllPendingReimbursment();
	public boolean updateApproveDeny(Reimbursment reimbursment);
}
