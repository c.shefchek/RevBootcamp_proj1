package com.revature.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.revature.model.Reimbursment;
import com.revature.util.ConnectionUtil;
import com.revature.util.LogUtil;

public class ReimbursmentDaoJdbc implements ReimbursmentDao {
	
	private static ReimbursmentDaoJdbc reimbursmentDaoJdbc;
	
	private ReimbursmentDaoJdbc(){
		
	}
	
	public static ReimbursmentDaoJdbc getReimbursmentDaoJdbc(){
		if(reimbursmentDaoJdbc == null){
			reimbursmentDaoJdbc = new ReimbursmentDaoJdbc();
			
		}
		return reimbursmentDaoJdbc;
	}
	/*
	private int r_id;
	private int s_id;
	private double amount;
	private int status;
	private String description;
	private String date_sub;
	private String date_app;
	private int approvedBy;
	private String image;
	private int type;
	 */

	@Override
	public boolean insertProcedure(Reimbursment reimbursment) {
		try(Connection connection = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			
			//Pay attention to this syntax
			String command = "{CALL INSERT_REIMBURSMENT(?,?,?,?,?)}";
			
			//Notice the CallableStatement
			CallableStatement statement = connection.prepareCall(command);
			
			//Set attributes to be inserted
			statement.setInt(++statementIndex, reimbursment.getS_id());
			statement.setDouble(++statementIndex, reimbursment.getAmount());
			statement.setString(++statementIndex, reimbursment.getDescription().toUpperCase());
			statement.setString(++statementIndex, reimbursment.getDate_sub());
			statement.setInt(++statementIndex, reimbursment.getType());
			
			if(statement.executeUpdate() > 0) {
				return true;
			}
			
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception creating a new REIMBURSMENT with stored procedure", e);
		}
		return false;
	}

	@Override
	public Reimbursment select(Reimbursment reimbursment) {
		try(Connection connection = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "SELECT * FROM REIMBURSMENT WHERE R_ID = ?";
			PreparedStatement statement = connection.prepareStatement(command);
			statement.setInt(++statementIndex, reimbursment.getR_id());
			ResultSet result = statement.executeQuery();

			while(result.next()) {
				return new Reimbursment(
						result.getInt("R_ID"), 
						result.getInt("S_ID"),
						result.getDouble("AMOUNT"),
						result.getInt("STATUS"),
						result.getString("DESC_RE"),
						result.getString("DATE_SUB"),
						result.getString("DATE_APP"),
						result.getInt("APP_BY"),
						result.getString("IMAGE"),
						result.getInt("RE_TYPE")
						);
			}
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception selecting a REIMBURSMENT", e);
		}
		return new Reimbursment();
	}

	@Override
	public List<Reimbursment> selectAll() {
		try(Connection connection = ConnectionUtil.getConnection()) {
			String command = "SELECT * FROM REIMBURSMENT";
			PreparedStatement statement = connection.prepareStatement(command);
			ResultSet result = statement.executeQuery();

			List<Reimbursment> reimbursmentList = new ArrayList<>();
			while(result.next()) {
				reimbursmentList.add(new Reimbursment(
						result.getInt("R_ID"), 
						result.getInt("S_ID"),
						result.getDouble("AMOUNT"),
						result.getInt("STATUS"),
						result.getString("DESC_RE"),
						result.getString("DATE_SUB"),
						result.getString("DATE_APP"),
						result.getInt("APP_BY"),
						"",							//XXX Need to handle the picture???
						result.getInt("RE_TYPE")
						));
			}
			
			return reimbursmentList;
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception selecting all Reimbursment", e);
		} 
		return new ArrayList<>();
	}

	@Override
	public List<Reimbursment> getStaffReimbursment(Reimbursment reimbursment) {
		try(Connection connection = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "SELECT * FROM REIMBURSMENT WHERE S_ID = ?";
			PreparedStatement statement = connection.prepareStatement(command);
			statement.setInt(++statementIndex, reimbursment.getS_id());
			ResultSet result = statement.executeQuery();

			List<Reimbursment> reimbursmentList = new ArrayList<>();
			while(result.next()) {
				reimbursmentList.add(new Reimbursment(
						result.getInt("R_ID"), 
						result.getInt("S_ID"),
						result.getDouble("AMOUNT"),
						result.getInt("STATUS"),
						result.getString("DESC_RE"),
						result.getString("DATE_SUB"),
						result.getString("DATE_APP"),
						result.getInt("APP_BY"),
						"",							//XXX Need to handle the picture???
						result.getInt("RE_TYPE")
						));
			}
			
			return reimbursmentList;
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception selecting all S_ID Reimbursments", e);
		} 
		return new ArrayList<>();
	}

	public List<Reimbursment> getResolvedReimbursment(Reimbursment reimbursment) {
		try(Connection connection = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "SELECT * FROM REIMBURSMENT WHERE S_ID = ? AND STATUS > 2";
			PreparedStatement statement = connection.prepareStatement(command);
			statement.setInt(++statementIndex, reimbursment.getS_id());
			ResultSet result = statement.executeQuery();

			List<Reimbursment> reimbursmentList = new ArrayList<>();
			while(result.next()) {
				reimbursmentList.add(new Reimbursment(
						result.getInt("R_ID"), 
						result.getInt("S_ID"),
						result.getDouble("AMOUNT"),
						result.getInt("STATUS"),
						result.getString("DESC_RE"),
						result.getString("DATE_SUB"),
						result.getString("DATE_APP"),
						result.getInt("APP_BY"),
						"",							//XXX Need to handle the picture???
						result.getInt("RE_TYPE")
						));
			}
			
			return reimbursmentList;
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception selecting all S_ID Reimbursments", e);
		} 
		return new ArrayList<>();
	}
	public List<Reimbursment> getPendingReimbursment(Reimbursment reimbursment) {
		try(Connection connection = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "SELECT * FROM REIMBURSMENT WHERE S_ID = ? AND STATUS < 3";
			PreparedStatement statement = connection.prepareStatement(command);
			statement.setInt(++statementIndex, reimbursment.getS_id());
			ResultSet result = statement.executeQuery();

			List<Reimbursment> reimbursmentList = new ArrayList<>();
			while(result.next()) {
				reimbursmentList.add(new Reimbursment(
						result.getInt("R_ID"), 
						result.getInt("S_ID"),
						result.getDouble("AMOUNT"),
						result.getInt("STATUS"),
						result.getString("DESC_RE"),
						result.getString("DATE_SUB"),
						result.getString("DATE_APP"),
						result.getInt("APP_BY"),
						"",							//XXX Need to handle the picture???
						result.getInt("RE_TYPE")
						));
			}
			
			return reimbursmentList;
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception selecting all S_ID Reimbursments", e);
		} 
		return new ArrayList<>();
	}

	@Override
	public List<Reimbursment> getAllResolvedReimbursment() {
		try(Connection connection = ConnectionUtil.getConnection()) {
			String command = "SELECT * FROM REIMBURSMENT WHERE STATUS > 2";
			PreparedStatement statement = connection.prepareStatement(command);
			ResultSet result = statement.executeQuery();

			List<Reimbursment> reimbursmentList = new ArrayList<>();
			while(result.next()) {
				reimbursmentList.add(new Reimbursment(
						result.getInt("R_ID"), 
						result.getInt("S_ID"),
						result.getDouble("AMOUNT"),
						result.getInt("STATUS"),
						result.getString("DESC_RE"),
						result.getString("DATE_SUB"),
						result.getString("DATE_APP"),
						result.getInt("APP_BY"),
						"",							//XXX Need to handle the picture???
						result.getInt("RE_TYPE")
						));
			}
			
			return reimbursmentList;
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception selecting all S_ID Reimbursments", e);
		} 
		return new ArrayList<>();
	}

	@Override
	public List<Reimbursment> getAllPendingReimbursment() {
		try(Connection connection = ConnectionUtil.getConnection()) {
			String command = "SELECT * FROM REIMBURSMENT WHERE STATUS < 3";
			PreparedStatement statement = connection.prepareStatement(command);
			ResultSet result = statement.executeQuery();

			List<Reimbursment> reimbursmentList = new ArrayList<>();
			while(result.next()) {
				reimbursmentList.add(new Reimbursment(
						result.getInt("R_ID"), 
						result.getInt("S_ID"),
						result.getDouble("AMOUNT"),
						result.getInt("STATUS"),
						result.getString("DESC_RE"),
						result.getString("DATE_SUB"),
						result.getString("DATE_APP"),
						result.getInt("APP_BY"),
						"",							//XXX Need to handle the picture???
						result.getInt("RE_TYPE")
						));
			}
			
			return reimbursmentList;
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception selecting all S_ID Reimbursments", e);
		} 
		return new ArrayList<>();
	}
	//Reimbursment(int r_id, int status, String date, int s_id)
	
	public boolean updateApproveDeny(Reimbursment reimbursment){
		try(Connection connection = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			
			String command = "{CALL UPDATE_STATUS_REIMBURSMENT(?, ?, ?)}";
			
			CallableStatement statement = connection.prepareCall(command);
			
			//Set attributes to be inserted
			statement.setInt(++statementIndex, reimbursment.getStatus());
			statement.setInt(++statementIndex, reimbursment.getApprovedBy());
			statement.setInt(++statementIndex, reimbursment.getR_id());
			int output = statement.executeUpdate();
			System.out.println("Output: " + output);
			if(output > 0) {
				return true;
			}
			
		} catch (SQLException e) {
			System.out.println("Catch: " + e);
			LogUtil.logger.warn("Exception creating a new REIMBURSMENT with stored procedure", e);
		}
		return false;
	}
}
