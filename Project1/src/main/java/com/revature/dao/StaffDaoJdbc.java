package com.revature.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.revature.model.Staff;
import com.revature.util.ConnectionUtil;
import com.revature.util.LogUtil;

public class StaffDaoJdbc implements StaffDao{
	
	/*Singleton transformation of JDBC implementation object */
	private static StaffDaoJdbc staffDaoJdbc;
	
	private StaffDaoJdbc(){
		
	}
	
	public static StaffDaoJdbc getStaffDaoJdbc(){
		if (staffDaoJdbc == null){
			staffDaoJdbc = new StaffDaoJdbc();
		}
		return staffDaoJdbc;
	}
	
	/*
	private int id;
	private String username;
	private String password;
	private int rank;
	private String fName;
	private String lName;
	private String phone;
	private String email;
	 */
	

	@Override
	public boolean insert(Staff staff) {
		try(Connection connection = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "INSERT INTO STAFF VALUES(NULL,?,?,?,?,?,?,?)";

			PreparedStatement statement = connection.prepareStatement(command);

			//Set attributes to be inserted
			statement.setString(++statementIndex, staff.getUsername().toLowerCase());
			statement.setString(++statementIndex, staff.getPassword());
			statement.setInt(++statementIndex, staff.getRank());
			statement.setString(++statementIndex, staff.getfName().toUpperCase());
			statement.setString(++statementIndex, staff.getlName().toUpperCase());
			statement.setString(++statementIndex, staff.getPhone());
			statement.setString(++statementIndex, staff.getEmail().toUpperCase());
			
			if(statement.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception creating a new STAFF", e);
		}
		return false;
	}

	@Override
	public boolean insertProcedure(Staff staff) {
		try(Connection connection = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			
			//Pay attention to this syntax
			String command = "{CALL INSERT_STAFF(?,?,?,?,?,?,?)}";
			
			//Notice the CallableStatement
			CallableStatement statement = connection.prepareCall(command);
			
			//Set attributes to be inserted
			statement.setString(++statementIndex, staff.getUsername().toLowerCase());
			statement.setString(++statementIndex, staff.getPassword());
			statement.setInt(++statementIndex, staff.getRank());
			statement.setString(++statementIndex, staff.getfName().toUpperCase());
			statement.setString(++statementIndex, staff.getlName().toUpperCase());
			statement.setString(++statementIndex, staff.getPhone());
			statement.setString(++statementIndex, staff.getEmail().toUpperCase());
			
			if(statement.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception creating a new STAFF with stored procedure", e);
		}
		return false;
	}

	@Override
	public Staff select(Staff staff) {
		try(Connection connection = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "SELECT * FROM STAFF WHERE S_USERNAME = ?";
			PreparedStatement statement = connection.prepareStatement(command);
			statement.setString(++statementIndex, staff.getUsername());
			ResultSet result = statement.executeQuery();

			while(result.next()) {
				return new Staff(result.getInt("S_ID"),
						result.getString("S_USERNAME"),
						result.getString("S_PASSWORD"),
						result.getInt("S_RANK"),
						result.getString("S_FNAME"),
						result.getString("S_LNAME"),
						result.getString("S_PHONE"),
						result.getString("S_EMAIL")
						);
			}
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception selecting all STAFF", e);
		}
		return new Staff();
	}

	@Override
	public List<Staff> selectAll() {
		try(Connection connection = ConnectionUtil.getConnection()) {
			String command = "SELECT * FROM STAFF";
			PreparedStatement statement = connection.prepareStatement(command);
			ResultSet result = statement.executeQuery();

			List<Staff> staffList = new ArrayList<>();
			while(result.next()) {
				staffList.add(new Staff(result.getInt("S_ID"),
						result.getString("S_USERNAME"),
						result.getString("S_PASSWORD"),
						result.getInt("S_RANK"),
						result.getString("S_FNAME"),
						result.getString("S_LNAME"),
						result.getString("S_PHONE"),
						result.getString("S_EMAIL")
						));
			}

			return staffList;
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception selecting all from STAFF", e);
		} 
		return new ArrayList<>();
	}

	@Override
	public String getStaffHash(Staff staff) {
		try(Connection connection = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "SELECT PASSWORD_HASH(?,?) AS HASH FROM DUAL";
			PreparedStatement statement = connection.prepareStatement(command);
			statement.setString(++statementIndex, staff.getUsername());
			statement.setString(++statementIndex, staff.getPassword());
			ResultSet result = statement.executeQuery();

			if(result.next()) {
				return result.getString("HASH");
			}
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception getting STAFF hash", e);
		} 
		return new String();
	}

	@Override
	public List<Staff> selectAllEmp() {
		try(Connection connection = ConnectionUtil.getConnection()) {
			String command = "SELECT * FROM STAFF WHERE S_RANK = 2";
			PreparedStatement statement = connection.prepareStatement(command);
			ResultSet result = statement.executeQuery();

			List<Staff> staffList = new ArrayList<>();
			while(result.next()) {
				staffList.add(new Staff(result.getInt("S_ID"),
						result.getString("S_USERNAME"),
						result.getString("S_PASSWORD"),
						result.getInt("S_RANK"),
						result.getString("S_FNAME"),
						result.getString("S_LNAME"),
						result.getString("S_PHONE"),
						result.getString("S_EMAIL")
						));
			}

			return staffList;
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception selecting all from STAFF", e);
		} 
		return new ArrayList<>();
	}
	public boolean updateProcedure(Staff staff) {
		try(Connection connection = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			
			//Pay attention to this syntax
			String command = "{CALL UPDATE_STAFF(?, ?, ?, ?, ?, ?)}";
			
			//Notice the CallableStatement
			CallableStatement statement = connection.prepareCall(command);
			
			//Set attributes to be inserted
			statement.setInt(++statementIndex, staff.getId());
			statement.setString(++statementIndex, staff.getUsername());
			statement.setString(++statementIndex, staff.getfName().toUpperCase());
			statement.setString(++statementIndex, staff.getlName().toUpperCase());
			statement.setString(++statementIndex, staff.getPhone());
			statement.setString(++statementIndex, staff.getEmail().toUpperCase());
			
			if(statement.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception UPDATING a STAFF with stored procedure", e);
		}
		return false;
	}

}
