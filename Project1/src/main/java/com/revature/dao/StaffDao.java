package com.revature.dao;

import java.util.List;

import com.revature.model.Staff;

public interface StaffDao {
	public boolean insert(Staff staff);
	public boolean insertProcedure(Staff staff);
	public Staff select(Staff staff);
	public List<Staff> selectAll();
	public String getStaffHash(Staff staff);
	public List<Staff> selectAllEmp();
	public boolean updateProcedure(Staff staff);
}
