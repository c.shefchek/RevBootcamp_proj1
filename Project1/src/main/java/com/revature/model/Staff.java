package com.revature.model;

public class Staff {
	private int id;
	private String username;
	private String password;
	private int rank;
	private String fName;
	private String lName;
	private String phone;
	private String email;
	
	public Staff() {
		this.username = "";
		this.password = "";
		this.rank = 0;
		this.fName = "";
		this.lName = "";
		this.phone = "";
		this.email = "";
	}
	
	public Staff(int id, String username, String password, int rank, String fName, String lName, String phone,
			String email) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.rank = rank;
		this.fName = fName;
		this.lName = lName;
		this.phone = phone;
		this.email = email;
	}
	
	public Staff(int id, String username, String fName, String lName, String phone, String email) {
		this.id = id;
		this.username = username;
		this.password = "";
		this.rank = 0;
		this.fName = fName;
		this.lName = lName;
		this.phone = phone;
		this.email = email;
	}

	public Staff(String username, String password) {
		this.username = username;
		this.password = password;

	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Staff [id=" + id + ", username=" + username + ", password=" + password + ", rank=" + rank + ", fName="
				+ fName + ", lName=" + lName + ", phone=" + phone + ", email=" + email + "]";
	}
	
	

}
