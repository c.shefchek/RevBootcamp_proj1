package com.revature.model;

public class Reimbursment {
	private int r_id;
	private int s_id;
	private double amount;
	private int status;
	private String description;
	private String date_sub;
	private String date_app;
	private int approvedBy;
	private String image;
	private int type;
	
	
	public Reimbursment() {
		this.r_id = 0;
		this.s_id = 0;
		this.amount = 0.0;
		this.status = 0;
		this.description = "";
		this.date_sub = "";
		this.date_app = "";
		this.approvedBy = 0;
		this.image = "";
		this.type = 0;
	}
	
	public Reimbursment(int s_id) {
		this.r_id = 0;
		this.s_id = s_id;
		this.amount = 0.0;
		this.status = 0;
		this.description = "";
		this.date_sub = "";
		this.date_app = "";
		this.approvedBy = 0;
		this.image = "";
		this.type = 0;
	}

	public Reimbursment(int r_id, int status, int s_id) {
		this.r_id = r_id;
		this.s_id = 0;
		this.amount = 0.0;
		this.status = status;
		this.description = "";
		this.date_sub = "";
		this.date_app = "";
		this.approvedBy = s_id;
		this.image = "";
		this.type = 0;
	}

	public Reimbursment(int r_id, int s_id, double amount, int status, String description, String date_sub,
			String date_app, int approvedBy, String image, int type) {
		this.r_id = r_id;
		this.s_id = s_id;
		this.amount = amount;
		this.status = status;
		this.description = description;
		this.date_sub = date_sub;
		this.date_app = date_app;
		this.approvedBy = approvedBy;
		this.image = "";
		this.type = type;
	}


	public int getR_id() {
		return r_id;
	}


	public void setR_id(int r_id) {
		this.r_id = r_id;
	}


	public int getS_id() {
		return s_id;
	}


	public void setS_id(int s_id) {
		this.s_id = s_id;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getDate_sub() {
		return date_sub;
	}


	public void setDate_sub(String date_sub) {
		this.date_sub = date_sub;
	}


	public String getDate_app() {
		return date_app;
	}


	public void setDate_app(String date_app) {
		this.date_app = date_app;
	}


	public int getApprovedBy() {
		return approvedBy;
	}


	public void setApprovedBy(int approvedBy) {
		this.approvedBy = approvedBy;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public int getType() {
		return type;
	}


	public void setType(int type) {
		this.type = type;
	}


	@Override
	public String toString() {
		return "Reimbursment [r_id=" + r_id + ", s_id=" + s_id + ", amount=" + amount + ", status=" + status
				+ ", description=" + description + ", date_sub=" + date_sub + ", date_app=" + date_app + ", approvedBy="
				+ approvedBy + ", image=" + image + ", type=" + type + "]";
	}
	
	
}
