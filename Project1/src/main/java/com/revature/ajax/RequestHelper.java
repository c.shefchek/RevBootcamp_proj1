package com.revature.ajax;

import javax.servlet.http.HttpServletRequest;

import com.revature.controller.AddEmpReimbursment;
import com.revature.controller.AddStaff;
import com.revature.controller.AllEmpReimbursment;
import com.revature.controller.AllEmployee;
import com.revature.controller.AllPendingReimbursment;
import com.revature.controller.AllResolvedReimbursment;
import com.revature.controller.GetEmpReimbursment;
import com.revature.controller.GetPendingReimbursment;
import com.revature.controller.GetResolvedReimbursment;
import com.revature.controller.UpdateStaff;
import com.revature.controller.UpdateStatus;
import com.revature.util.FinalUtil;

public class RequestHelper {
	
	public static Object process(HttpServletRequest req){
		switch(req.getRequestURI()){
		case"/Project1/getEmpReimbursment.ajax":
			return GetEmpReimbursment.getReimbursment(req);
		case"/Project1/addEmpReimbursment.ajax":
			return AddEmpReimbursment.createReimbursment(req);
		case"/Project1/allEmpReimbursment.ajax":
			return AllEmpReimbursment.allEmpReimbursment();
		case"/Project1/addStaff.ajax":
			return AddStaff.createStaff(req);
		case"/Project1/getPendingReimbursment.ajax":
			return GetPendingReimbursment.getPending(req);
		case"/Project1/getResolvedReimbursment.ajax":
			return GetResolvedReimbursment.getResolved(req);
		case"/Project1/allResolvedReimbursment.ajax":
			return AllResolvedReimbursment.allResolvedReimbursment();
		case"/Project1/allPendingReimbursment.ajax":
			return AllPendingReimbursment.allPendingReimbursment();
		case"/Project1/allEmployees.ajax":
			return AllEmployee.allEmployee();
		case"/Project1/updateStatus.ajax":
			return UpdateStatus.updateStatus(req);
		case"/Project1/updateStaff.ajax":
			return UpdateStaff.updateStaff(req);
		default:
			return new AjaxMessage(FinalUtil.NOT_IMPLEMENTED_CODE, FinalUtil.NOT_IMPLEMENTED);
		}
	}
}