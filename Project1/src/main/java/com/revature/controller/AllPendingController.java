package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

public class AllPendingController {
	public static String allPending(HttpServletRequest request){
		
		
		if (request.getSession().getAttribute("loggedStaff") == null){
			return "index.html";
		}
		return "managerPending.jsp";
		
	}
}
