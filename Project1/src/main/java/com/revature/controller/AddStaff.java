package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

import com.revature.model.Reimbursment;
import com.revature.model.Staff;
import com.revature.service.ReimbursmentService;
import com.revature.service.StaffService;

public class AddStaff {
	public static Object createStaff(HttpServletRequest request){

		
		StaffService.getStaffService().registerStaffSecure(
				new Staff(0, 
						request.getParameter("username"),
						request.getParameter("password"), 
						Integer.parseInt(request.getParameter("rank")),
						request.getParameter("fname").toUpperCase(),
						request.getParameter("lname").toUpperCase(), 
						request.getParameter("phone"), 
						request.getParameter("email").toUpperCase())
				);
		
		return null;
		
	}
}
