package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

public class AllEmployeeController {
	public static String allEmployee(HttpServletRequest request){
		
		
		if (request.getSession().getAttribute("loggedStaff") == null){
			return "index.html";
		}
		return "managerAllEmp.jsp";
		
	}
}
