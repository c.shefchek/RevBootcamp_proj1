package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

import com.revature.model.Reimbursment;
import com.revature.service.ReimbursmentService;

public class UpdateStatus {
	public static Object updateStatus(HttpServletRequest request){
	
		ReimbursmentService.getReimbursmentService().updateApproveDeny(
				new Reimbursment(Integer.parseInt(request.getParameter("r_id")), 
						Integer.parseInt(request.getParameter("status")), 
						Integer.parseInt(request.getParameter("manager"))));
		return null;
		
	}
}
