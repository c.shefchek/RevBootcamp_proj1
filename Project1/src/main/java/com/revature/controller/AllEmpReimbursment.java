package com.revature.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Reimbursment;
import com.revature.service.ReimbursmentService;

public class AllEmpReimbursment {
	// ReimbursmentService.getReimbursmentService().listAllReimbursments()
	
	public static Object allEmpReimbursment(){
	    final ObjectMapper mapper = new ObjectMapper();
	    
	    String jsonSting = "";
	    
	    try {
			jsonSting = mapper.writeValueAsString(ReimbursmentService.getReimbursmentService().listAllReimbursments());
		} catch (NumberFormatException | JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	    return jsonSting;
	}
}
