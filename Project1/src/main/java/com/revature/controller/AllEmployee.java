package com.revature.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.service.StaffService;

public class AllEmployee {
	public static Object allEmployee() {
		final ObjectMapper mapper = new ObjectMapper();

		String jsonSting = "";

		try {
			jsonSting = mapper.writeValueAsString(StaffService.getStaffService().listAllEmployee());
		} catch (NumberFormatException | JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonSting;

	}
}
