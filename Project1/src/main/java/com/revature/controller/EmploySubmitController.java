package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

public class EmploySubmitController {
	public static String employeeSubmit(HttpServletRequest request){

		if (request.getSession().getAttribute("loggedStaff") == null){
			return "index.html";
		}
		return "empSubReq.jsp";
		
	}
}
