package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

public class EmpPendingReqController {
	
	public static String empPending(HttpServletRequest request){
		
		
		if (request.getSession().getAttribute("loggedStaff") == null){
			return "index.html";
		}
		return "pendingReq.jsp";
		
	
	}
}
