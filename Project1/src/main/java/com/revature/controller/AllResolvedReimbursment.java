package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Reimbursment;
import com.revature.service.ReimbursmentService;

public class AllResolvedReimbursment {
	public static Object allResolvedReimbursment() {
		final ObjectMapper mapper = new ObjectMapper();

		String jsonSting = "";

		try {
			jsonSting = mapper.writeValueAsString(ReimbursmentService.getReimbursmentService()
					.listAllResolvedReimbursments());
		} catch (NumberFormatException | JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonSting;

	}
}
