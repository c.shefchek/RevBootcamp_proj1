package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

import com.revature.model.Reimbursment;
import com.revature.model.Staff;
import com.revature.service.ReimbursmentService;
import com.revature.service.StaffService;

public class AddEmpReimbursment {
	public static Object createReimbursment(HttpServletRequest request){

		ReimbursmentService.getReimbursmentService().registerReimbursmentSecure(
				new Reimbursment(0, Integer.parseInt(request.getParameter("id")), 
						Double.parseDouble(request.getParameter("amount")), 
			1, request.getParameter("desc"), request.getParameter("date"), 
			"", 0,"", Integer.parseInt(request.getParameter("type"))));
		
		return null;
		
	}
}
