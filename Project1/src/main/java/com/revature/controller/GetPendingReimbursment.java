package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Reimbursment;
import com.revature.service.ReimbursmentService;

public class GetPendingReimbursment {
	public static Object getPending(HttpServletRequest request) {
		final ObjectMapper mapper = new ObjectMapper();

		String jsonSting = "";

		try {
			jsonSting = mapper.writeValueAsString(ReimbursmentService.getReimbursmentService()
					.listPendingReimbursments(new Reimbursment(Integer.parseInt(request.getParameter("s_id")))));
		} catch (NumberFormatException | JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonSting;

	}
}
