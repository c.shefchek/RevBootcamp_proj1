package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

public class AllResolvedController {
	public static String allResolved(HttpServletRequest request){
		
		
		if (request.getSession().getAttribute("loggedStaff") == null){
			return "index.html";
		}
		return "managerResolved.jsp";
		
	}
}
