package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

import com.revature.model.Staff;
import com.revature.service.StaffService;
import com.revature.util.FinalUtil;

public class LoginController {
	
	public static String login(HttpServletRequest request){
		if(request.getMethod().equals(FinalUtil.HTTP_Get)) return "index.html";

		Staff loggedStaff = StaffService.getStaffService().login(
				new Staff(
						request.getParameter("username"),
						request.getParameter("password")
						));
		if (loggedStaff.getUsername().equals("")){
			return "index.html";
		} else{
			//REturn another URI and create controller that handles that 
			//return "/customer.do"; //The DAO method
		}
		request.getSession().setAttribute("loggedStaff", loggedStaff);
		if(loggedStaff.getRank() == 1){
			return "manager.jsp";
		}else if (loggedStaff.getRank() == 2){
			return "employee.jsp";
		}
		return "index.html";
	}

}
