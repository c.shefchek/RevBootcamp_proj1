package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

public class ManagerHomeController {
	public static String managerHome(HttpServletRequest request){
		
		
		if (request.getSession().getAttribute("loggedStaff") == null){
			return "index.html";
		}
		return "manager.jsp";
		
	}
}
