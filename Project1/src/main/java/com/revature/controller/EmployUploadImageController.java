package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

public class EmployUploadImageController {
	public static String uploadImage(HttpServletRequest request){
		
		
		if (request.getSession().getAttribute("loggedStaff") == null){
			return "index.html";
		}
		return "uploadImage.jsp";
		
	
	}
}
