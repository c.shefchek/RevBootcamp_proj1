package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

public class EmployeeHomeController {
	public static String employeeHome(HttpServletRequest request){

		if (request.getSession().getAttribute("loggedStaff") == null){
			return "index.html";
		}
		return "employee.jsp";
		
	}
}
