package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

public class EmployeeViewController {
	public static String employeeView(HttpServletRequest request){
		
		
		if (request.getSession().getAttribute("loggedStaff") == null){
			return "index.html";
		}
		return "personalView.jsp";
		
	}
}
