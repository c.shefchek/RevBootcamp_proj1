package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

import com.revature.model.Staff;
import com.revature.service.StaffService;

public class UpdateStaff {
	
	public static Object updateStaff(HttpServletRequest request){
		Staff toUpdate = new Staff(Integer.parseInt(
				request.getParameter("s_id")),
				request.getParameter("username"), 
				request.getParameter("fname"),
				request.getParameter("lname"), 
				request.getParameter("phone"), 
				request.getParameter("email"));
		
		StaffService.getStaffService().updateProcedureStaff(toUpdate);
		
		Staff loggedStaff = (Staff) request.getSession().getAttribute("loggedStaff");
		if(request.getParameter("username") != ""){
			loggedStaff.setUsername(request.getParameter("username"));
		}
		if(request.getParameter("fname") != ""){
			loggedStaff.setfName(request.getParameter("fname"));
		}
		if(request.getParameter("lname") != ""){
			loggedStaff.setlName(request.getParameter("lname"));
		}
		if(request.getParameter("phone") != ""){
			loggedStaff.setPhone(request.getParameter("phone"));
		}
		if(request.getParameter("email") != ""){
			loggedStaff.setEmail(request.getParameter("email"));
		}
		
		request.getSession().setAttribute("loggedStaff", loggedStaff); 
		
		return null;
		
	}
}
