package com.revature.service;

import java.util.List;

import com.revature.dao.StaffDaoJdbc;
import com.revature.model.Staff;

public class StaffService {
	
	private static StaffService staffService;
	
	private StaffService(){

	}
	
	public static StaffService getStaffService(){
		if (staffService == null){
			staffService = new StaffService();
		}
		return staffService;
	}

	
	/* Calls the insert method of the DAO */
	public boolean registerStaff(Staff staff) {
		
		return StaffDaoJdbc.getStaffDaoJdbc().insert(staff);
	}
	
	/* Calls the insert method that uses stored procedure in DAO */
	public boolean registerStaffSecure(Staff staff) {
		return StaffDaoJdbc.getStaffDaoJdbc().insertProcedure(staff);
	}
	
	/* Calls selectAll method of the DAO */
	public List<Staff> listAllUsers() {
		return StaffDaoJdbc.getStaffDaoJdbc().selectAll();
	}
	
	/* Calls select method of the DAO by username and performs login logic */
	public Staff login(Staff staff) {
		//Get user information without validating
		Staff loggedStaff = StaffDaoJdbc.getStaffDaoJdbc().select(staff);
		
		/*
		 * loggedStaff.getPassword() is the hash we have stored.
		 * We compare that against the hash of the user input.
		 * If credentials are correct we return the full Staff information.
		 * Else, we return a blank object.
		 */
		if(loggedStaff.getPassword().equals(StaffDaoJdbc.getStaffDaoJdbc().getStaffHash(staff))) {
			return loggedStaff;
		}
		
		return new Staff();
	}
	
	//Executes select method on DAO to check if a Staff exists
	public boolean isUserNameTaken(Staff staff){
		if(StaffDaoJdbc.getStaffDaoJdbc().select(staff).getUsername().equals("")){
			return false;
		}
		else return true;
	}
	public List<Staff> listAllEmployee() {
		return StaffDaoJdbc.getStaffDaoJdbc().selectAllEmp();
	}
	public boolean updateProcedureStaff(Staff staff) {
		
		return StaffDaoJdbc.getStaffDaoJdbc().updateProcedure(staff);
	}

}
