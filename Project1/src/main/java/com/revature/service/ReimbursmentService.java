package com.revature.service;

import java.util.List;

import com.revature.dao.ReimbursmentDaoJdbc;
import com.revature.model.Reimbursment;

public class ReimbursmentService {

	private static ReimbursmentService reimbursmentService;
	
	private ReimbursmentService(){

	}
	
	public static ReimbursmentService getReimbursmentService(){
		if (reimbursmentService == null){
			reimbursmentService = new ReimbursmentService();
		}
		return reimbursmentService;
	}

	
	/* Calls the insert method that uses stored procedure in DAO */
	public boolean registerReimbursmentSecure(Reimbursment reimbursment) {
		return ReimbursmentDaoJdbc.getReimbursmentDaoJdbc().insertProcedure(reimbursment);
	}
	
	/* Calls selectAll method of the DAO */
	public List<Reimbursment> listAllReimbursments() {
		return ReimbursmentDaoJdbc.getReimbursmentDaoJdbc().selectAll();
	}
	
	/* Calls selectAll method of the DAO */
	public List<Reimbursment> listEmpReimbursments(Reimbursment reimbursment) {
		return ReimbursmentDaoJdbc.getReimbursmentDaoJdbc().getStaffReimbursment(reimbursment);
	}
	
	/* Calls selectResolved method of the DAO */
	public List<Reimbursment> listResolvedReimbursments(Reimbursment reimbursment) {
		return ReimbursmentDaoJdbc.getReimbursmentDaoJdbc().getResolvedReimbursment(reimbursment);
	}
	
	/* Calls selectPending method of the DAO */
	public List<Reimbursment> listPendingReimbursments(Reimbursment reimbursment) {
		return ReimbursmentDaoJdbc.getReimbursmentDaoJdbc().getPendingReimbursment(reimbursment);
	}
	
	/* Calls selectAllResolved method of the DAO */
	public List<Reimbursment> listAllResolvedReimbursments() {
		return ReimbursmentDaoJdbc.getReimbursmentDaoJdbc().getAllResolvedReimbursment();
	}
	
	/* Calls selectAllPending method of the DAO */
	public List<Reimbursment> listAllPendingReimbursments() {
		return ReimbursmentDaoJdbc.getReimbursmentDaoJdbc().getAllPendingReimbursment();
	}
	
	public boolean updateApproveDeny(Reimbursment reimbursment) {
		return ReimbursmentDaoJdbc.getReimbursmentDaoJdbc().updateApproveDeny(reimbursment);
	}
	
	
	
}
