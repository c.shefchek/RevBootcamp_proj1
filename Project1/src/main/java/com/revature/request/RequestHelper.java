package com.revature.request;

import javax.servlet.http.HttpServletRequest;

import com.revature.controller.AllEmployeeController;
import com.revature.controller.AllPendingController;
import com.revature.controller.AllResolvedController;
import com.revature.controller.EmpGetREsolvedController;
import com.revature.controller.EmpPendingReqController;
import com.revature.controller.EmploySubmitController;
import com.revature.controller.EmployUploadImageController;
import com.revature.controller.EmployeeHomeController;
import com.revature.controller.EmployeeViewController;
import com.revature.controller.LoginController;
import com.revature.controller.LogoutController;
import com.revature.controller.ManagerHomeController;

public class RequestHelper {
	
	public static String process(HttpServletRequest request){
		switch(request.getRequestURI()){
		case "/Project1/login.do":
			return LoginController.login(request);
		case "/Project1/empsubmit.do":
			return EmploySubmitController.employeeSubmit(request);
		case "/Project1/emphome.do":
			return EmployeeHomeController.employeeHome(request);
		case "/Project1/getpending.do":
			return EmpPendingReqController.empPending(request);
		case "/Project1/getresolved.do":
			return EmpGetREsolvedController.getREsolved(request);
		case "/Project1/uploadimage.do":
			return EmployUploadImageController.uploadImage(request);
		case "/Project1/empView.do":
			return EmployeeViewController.employeeView(request);
		case "/Project1/manhome.do":
			return ManagerHomeController.managerHome(request);
		case "/Project1/allpending.do":
			return AllPendingController.allPending(request);
		case "/Project1/allresolved.do":
			return AllResolvedController.allResolved(request);
		case "/Project1/allemp.do":
			return AllEmployeeController.allEmployee(request);
		case "/Project1/logout.do":
			return LogoutController.logout(request);
		default:
			return "404.jsp";
		}
	}

}
