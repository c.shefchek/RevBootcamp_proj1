window.onload = function(){
	document.getElementById("updateInfo").addEventListener("click", hideUpdate);
	document.getElementById("updater").style.visibility = "hidden";
	
	document.getElementById("updater").addEventListener("click", updateStaff);
	document.getElementById("clear").addEventListener("click", clearFields);
}
function updateStaff(){
	console.log("UPDATING");
	var fname = document.getElementById("fname").value;
	var lname = document.getElementById("lname").value;
	var username = document.getElementById("username").value;
	var phone = document.getElementById("phone").value;
	var email = document.getElementById("email").value;
	var s_id = document.getElementById("sessionInfo").innerHTML;


		// ________________________ Ajax _______________________//
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				location.reload();
			}
		}

		xhttp.open("POST",
				"http://localhost:8080/Project1/updateStaff.ajax?fname=" + fname
						+ "&lname=" + lname + "&username=" + username
						+ "&phone=" + phone
						+ "&email=" + email
						+ "&s_id=" + s_id, true);

		xhttp.send();

		// ________________________ Clear Form Fields _______________________//
		clearFields();
	
}

function hideUpdate(){
	var fields = document.getElementById("updater")
	if(fields.style.visibility == "hidden"){
		fields.style.visibility = "visible";
	}else fields.style.visibility = "hidden";
}

function clearFields() {
	document.getElementById("fname").value = "";
	document.getElementById("lname").value = "";
	document.getElementById("username").value = "";
	document.getElementById("phone").value = "";
	document.getElementById("email").value = "";

	document.getElementById("fname").value = "";
	document.getElementById("lname").value = "";
	document.getElementById("username").value = "";
	document.getElementById("phone").value = "";
	document.getElementById("email").value = "";
}