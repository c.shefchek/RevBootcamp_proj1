/**
 * 
 */

window.onload = function(){
	document.getElementById("newReimbursment").addEventListener("click", setNewRe);
	document.getElementById("clear").addEventListener("click", clearFields);
}
function setNewRe(){
	var amount = document.getElementById("amount").value;
	var desc= document.getElementById("desc").value;
	var type = document.getElementById("type").value;

	// ________________________ Parameters _______________________//
	
	var s_id = document.getElementById("sessionInfo").innerHTML;
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; // January is 0!
	var yyyy = today.getFullYear();
	if(dd<10) {
	    dd = '0'+dd
	} 
	if(mm<10) {
	    mm = '0'+mm
	} 
	var date = "" + mm + "/" + dd + "/" + yyyy;
	
	if(amount != "" && desc != "" && s_id != "" && type){
		if(typeof(amount).toLowerCase() != "number" && 
				typeof(s_id).toLowerCase() != "number" && typeof(type).toLowerCase() != "number"){
			// ________________________ Ajax _______________________//
			var xhttp = new XMLHttpRequest();
			
			xhttp.onreadystatechange = function(){
				if(xhttp.readyState == 4 && xhttp.status == 200){

				}
			}
			xhttp.open("POST", "http://localhost:8080/Project1/addEmpReimbursment.ajax?id="+s_id+"&amount="+amount+"&desc="+desc+"&type="+type+"&date="+date, true);
		
			xhttp.send();
		}
		// ________________________ Clear Form Fields _______________________//
		clearFields();
	}

}


function clearFields(){
	document.getElementById("amount").value = "";
	document.getElementById("desc").value  = "";
	document.getElementById("type").value = "";
	
	// Second time brings back the placeholder
	document.getElementById("amount").value = "";
	document.getElementById("desc").value  = "";
}

