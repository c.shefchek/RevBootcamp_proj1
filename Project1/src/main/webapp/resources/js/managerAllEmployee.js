window.onload = function() {
	displayAll();
	document.getElementById("newStaff").addEventListener("click",
			createNewStaff);
	document.getElementById("clear").addEventListener("click", clearFields);
}

function createNewStaff() {
	var fname = document.getElementById("fname").value;
	var lname = document.getElementById("lname").value;
	var username = document.getElementById("username").value;
	var password = document.getElementById("password").value;
	var phone = document.getElementById("phone").value;
	var email = document.getElementById("email").value;
	var rank = document.getElementById("rank").value;
	console.log("In function");
	if (lname != "" && fname != "" && username != "" && password != "" && phone != "" && email != "" && rank) {
		console.log("In if");
		// ________________________ Ajax _______________________//
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				// JSON /// NEED TO CLEAN UP XXX
				var ajaxObject = JSON.parse(xhttp.responseText);
				// How do I want to handle the message
				messageHandler(ajaxObject);

				// ________________________ Refresh page table
				// _______________________//
				displayAll();
			}
		}

		xhttp.open("POST",
				"http://localhost:8080/Project1/addStaff.ajax?fname=" + fname
						+ "&lname=" + lname + "&username=" + username
						+ "&password=" + password + "&phone=" + phone
						+ "&email=" + email + "&rank=" + rank, true);

		xhttp.send();

		// ________________________ Clear Form Fields _______________________//
		clearFields();
	}
}

function clearFields() {
	document.getElementById("fname").value = "";
	document.getElementById("lname").value = "";
	document.getElementById("username").value = "";
	document.getElementById("password").value = "";
	document.getElementById("phone").value = "";
	document.getElementById("email").value = "";
	document.getElementById("rank").value = "";

	document.getElementById("fname").value = "";
	document.getElementById("lname").value = "";
	document.getElementById("username").value = "";
	document.getElementById("password").value = "";
	document.getElementById("phone").value = "";
	document.getElementById("email").value = "";
}

function displayAll() {

	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			// JSON /// NEED TO CLEAN UP XXX
			var ajaxObject = JSON.parse(xhttp.responseText);
			var sjon = JSON.parse(ajaxObject);

			// setValues(ajaxObject);
			setValues(sjon);
		}
	}
	xhttp
			.open("POST", "http://localhost:8080/Project1/allEmployees.ajax",
					true);
	xhttp.send();
}

function messageHandler(response) {
	//console.log(response);
}

function setValues(response) { // Message is a json

	var tableElement = document.getElementById("newtable");
	tableElement.innerHTML = '';

	headerSet();
	for (var i = 0; i < response.length; i++) {

		var trElement = document.createElement("tr");
		var idElement = document.createElement("td");
		var userNameValue = document.createElement("td");
		var rankValue = document.createElement("td");
		var fNameValue = document.createElement("td");
		var lNameValue = document.createElement("td");
		var phoneValue = document.createElement("td");
		var emailValue = document.createElement("td");

		idElement.textContent = response[i].id;
		userNameValue.textContent = response[i].username;
		rankValue.textContent = response[i].rank;
		fNameValue.textContent = response[i].fName;
		lNameValue.textContent = response[i].lName;
		phoneValue.textContent = response[i].phone;
		emailValue.textContent = response[i].email;

		trElement.appendChild(idElement);
		trElement.appendChild(fNameValue);
		trElement.appendChild(lNameValue);
		trElement.appendChild(userNameValue);
		trElement.appendChild(rankValue);
		trElement.appendChild(phoneValue);
		trElement.appendChild(emailValue);

		var tableElement = document.getElementById("newtable");
		tableElement.appendChild(trElement);
	}
	// document.getElementById("message").innerHTML = response;

}

function headerSet() {
	var trElement = document.createElement("tr");
	var idElement = document.createElement("th");
	var fNameValue = document.createElement("th");
	var lNameValue = document.createElement("th");
	var userNameValue = document.createElement("th");
	var rankValue = document.createElement("th");
	var phoneValue = document.createElement("th");
	var emailValue = document.createElement("th");

	idElement.textContent = "Staff ID";
	fNameValue.textContent = "First Name";
	lNameValue.textContent = "Last Name";
	userNameValue.textContent = "Username";
	rankValue.textContent = "Rank";
	phoneValue.textContent = "Phone Number";
	emailValue.textContent = "Email";

	trElement.appendChild(idElement);
	trElement.appendChild(fNameValue);
	trElement.appendChild(lNameValue);
	trElement.appendChild(userNameValue);
	trElement.appendChild(rankValue);
	trElement.appendChild(phoneValue);
	trElement.appendChild(emailValue);

	var tableElement = document.getElementById("newtable");
	tableElement.appendChild(trElement);
}