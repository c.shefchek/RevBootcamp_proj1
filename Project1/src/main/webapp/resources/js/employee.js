/**
 * 
 */

window.onload = function(){
	document.getElementById("newReimbursment").addEventListener("click", setNewRe);
	document.getElementById("clear").addEventListener("click", clearFields);
	getDataRe();
}
function setNewRe(){
	var amount = document.getElementById("amount").value;
	var desc= document.getElementById("desc").value;
	var type = document.getElementById("type").value;
	console.log("Amount " + amount + " desc " + desc + " value " + type);

	//________________________ Parameters _______________________//
	
	var s_id = document.getElementById("sessionInfo").innerHTML;
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	if(dd<10) {
	    dd = '0'+dd
	} 
	if(mm<10) {
	    mm = '0'+mm
	} 
	var date = "" + mm + "/" + dd + "/" + yyyy;
	console.log(date + " " + typeof(date));
	

	
	//________________________ Ajax _______________________//
	var xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState == 4 && xhttp.status == 200){
			//JSON /// NEED TO CLEAN UP XXX
			var ajaxObject = JSON.parse(xhttp.responseText);
			// How do I want to handle the message
			messageHandler(ajaxObject);
			

			//________________________ Refresh page table _______________________//
			
			getDataRe();
		}
	}
	xhttp.open("POST", "http://localhost:8080/Project1/addEmpReimbursment.ajax?id="+s_id+"&amount="+amount+"&desc="+desc+"&type="+type+"&date="+date, true);

	xhttp.send();
	
	//________________________ Clear Form Fields _______________________//
	clearFields();

}

function messageHandler(response){
	console.log(response);
}

function clearFields(){
	document.getElementById("amount").value = "";
	document.getElementById("desc").value  = "";
	document.getElementById("type").value = "";
	
	//Second time brings back the placeholder
	document.getElementById("amount").value = "";
	document.getElementById("desc").value  = "";
}


function getDataRe(){
	console.log("getDataRe");
	var s_id = document.getElementById("sessionInfo").innerHTML;

	var xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState == 4 && xhttp.status == 200){
			//JSON /// NEED TO CLEAN UP XXX
			var ajaxObject = JSON.parse(xhttp.responseText);
			var sjon = JSON.parse(ajaxObject);
			//console.log("THIS: " + sjon[0]);
			//setValues(ajaxObject);
			setValues(sjon);
		}
	}
	xhttp.open("POST", "http://localhost:8080/Project1/getEmpReimbursment.ajax?s_id="+s_id, true);
	xhttp.send();
}

function setValues(response){ //Message is a json
	
	var tableElement = document.getElementById("newtable");
	tableElement.innerHTML = '';
	
	headerSet();
	for(var i = 0; i < response.length; i++){
		var trElement = document.createElement("tr");
		var reIdValue = document.createElement("td");
		var staffIdValue = document.createElement("td");
		var amountValue = document.createElement("td");
		var statusValue = document.createElement("td");
		var descValue = document.createElement("td");
		var dSubValue = document.createElement("td");
		var dAppValue = document.createElement("td");
		var appByValue = document.createElement("td");
		var imageValue = document.createElement("td");
		var typeValue = document.createElement("td");
		
		reIdValue.textContent = response[i].r_id;
		staffIdValue.textContent = response[i].s_id;
		amountValue.textContent = response[i].amount;
		statusValue.textContent = response[i].status;
		descValue.textContent = response[i].description;
		dSubValue.textContent = response[i].date_sub;
		dAppValue.textContent = response[i].date_app;
		appByValue.textContent = response[i].approvedBy;
		imageValue.textContent = response[i].image;
		typeValue.textContent = response[i].type;
		
		trElement.appendChild(reIdValue);
		trElement.appendChild(staffIdValue);
		trElement.appendChild(amountValue);
		trElement.appendChild(statusValue);
		trElement.appendChild(descValue);
		trElement.appendChild(dSubValue);
		trElement.appendChild(dAppValue);
		trElement.appendChild(appByValue);
		trElement.appendChild(imageValue);
		trElement.appendChild(typeValue);
		
		var tableElement = document.getElementById("newtable");
		tableElement.appendChild(trElement);
	}
	//document.getElementById("message").innerHTML = response;
	
}

function headerSet(){
	var trElement = document.createElement("tr");
	var reIdValue = document.createElement("th");
	var staffIdValue = document.createElement("th");
	var amountValue = document.createElement("th");
	var statusValue = document.createElement("th");
	var descValue = document.createElement("th");
	var dSubValue = document.createElement("th");
	var dAppValue = document.createElement("th");
	var appByValue = document.createElement("th");
	var imageValue = document.createElement("th");
	var typeValue = document.createElement("th");
	
	reIdValue.textContent = "Reimbursment ID";
	staffIdValue.textContent = "Staff ID";
	amountValue.textContent = "Amount";
	statusValue.textContent = "Status";
	descValue.textContent = "Description";
	dSubValue.textContent = "Date Submitted";
	dAppValue.textContent = "Date Approved";
	appByValue.textContent = "Approved By";
	imageValue.textContent = "Image";
	typeValue.textContent = "Type";
	
	trElement.appendChild(reIdValue);
	trElement.appendChild(staffIdValue);
	trElement.appendChild(amountValue);
	trElement.appendChild(statusValue);
	trElement.appendChild(descValue);
	trElement.appendChild(dSubValue);
	trElement.appendChild(dAppValue);
	trElement.appendChild(appByValue);
	trElement.appendChild(imageValue);
	trElement.appendChild(typeValue);
	
	var tableElement = document.getElementById("newtable");
	tableElement.appendChild(trElement);
}