<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>


<body>
	<nav class="navbar navbar-light bg-faded well well-sm">
	<ul class="nav navbar-nav">
		<li >
			<form class="form-inline"  method="post" action="emphome.do">
				<button class="btn btn-primary my-2 my-sm-0 active" type="submit">Home</button>
			</form>
		</li>
		<li>
			<form class="form-inline"  method="post" action="empsubmit.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">reimbursement
					request</button>
			</form>
		</li>
		<li>
			<form class="form-inline"  method="post" action="getpending.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">pending
					reimbursement</button>
			</form>
		</li>
		<li>
			<form class="form-inline"  method="post" action="getresolved.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">resolved
					reimbursement</button>
			</form>
		</li>
		<li>
			<form class="form-inline" method="post" action="uploadimage.do">
				<button class="btn btn-primary" type="submit">upload
					an image</button>
			</form>
		</li>
	</ul>
	<ul class="nav navbar-nav navbar-right">
		<li>
			<form class="form-inline"  method="post" action="empView.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">View Information</button>
			</form>
		</li>
		<li>
			<form class="form-inline"  method="post" action="logout.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">Log Out</button>
			</form>
		</li>
	</ul>
	</nav>

	<h1>Employee Page</h1>
<h6>Welcome the the employee reimbursement system. Use the links above to view requests or submit a new on.</h6>

</body>


</html>