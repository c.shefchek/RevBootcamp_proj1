<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.js"
	integrity="sha256-+f0njwC9E3IT9zDPry5DSIcGdSxQYezBaStQ4L0ZRfU="
	crossorigin="anonymous"></script>
<script type="text/javascript"
	src="https://code.angularjs.org/1.6.1/angular-route.js"></script>


<body>
	<nav class="navbar navbar-light bg-faded well well-sm">
	<ul class="nav navbar-nav">
		<li>
			<form class="form-inline" method="post" action="emphome.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">Home</button>
			</form>
		</li>
		<li>
			<form class="form-inline" method="post" action="empsubmit.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">reimbursement
					request</button>
			</form>
		</li>
		<li>
			<form class="form-inline" method="post" action="getpending.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">pending
					reimbursement</button>
			</form>
		</li>
		<li>
			<form class="form-inline" method="post" action="getresolved.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">resolved
					reimbursement</button>
			</form>
		</li>
		<li>
			<form class="form-inline" method="post" action="uploadimage.do">
				<button class="btn btn-primary" type="submit">upload an
					image</button>
			</form>
		</li>
	</ul>
	<ul class="nav navbar-nav navbar-right">
		<li>
			<form class="form-inline" method="post" action="empView.do">
				<button class="btn btn-primary my-2 my-sm-0 active" type="submit">View
					Information</button>
			</form>
		</li>
		<li>
			<form class="form-inline" method="post" action="logout.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">Log
					Out</button>
			</form>
		</li>
	</ul>
	</nav>

	<h1>Employee Page</h1>

	<h3>Name: ${ loggedStaff.fName} ${loggedStaff.lName}</h3>
	<h4>Username: ${ loggedStaff.username}</h4>
	<h4>Phone Number: ${ loggedStaff.phone}</h4>
	<h4>Email: ${ loggedStaff.email}</h4>

	<button class="btn btn-primary my-2 my-sm-0" id="updateInfo">Update</button>
	
	<div id="sessionInfo" hidden>${ loggedStaff.id }</div>

	<form id="updater">
		<input id="fname" type="text" placeholder="First Name" value="" /> 
		<input id="lname" type="text" placeholder="Last Name" /> 
		<input id="username" type="text" placeholder="Username" />
		<!-- <input id="password" type="text" placeholder="Password" />  -->
		<input id="phone" type="text" placeholder="Phone Number" /> 
		<input id="email" type="text" placeholder="Email" /> 
		<input id="updateInfo" type="button" value="Submit" /> 
		<input id="clear" type="button" value="Clear" />
	</form>

<script type="text/javascript" src="./resources/js/updateInfo.js"></script>
</body>

</html>