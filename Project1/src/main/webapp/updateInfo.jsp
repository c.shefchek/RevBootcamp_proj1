<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Manager</title>
</head>
<body>

<nav class="navbar navbar-light bg-faded well well-sm">
	<ul class="nav navbar-nav">
		<li >
			<form class="form-inline"  method="post" action="manhome.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">Home</button>
			</form>
		</li>
		<li>
			<form class="form-inline"  method="post" action="allpending.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">all pending</button>
			</form>
		</li>
		<li>
			<form class="form-inline"  method="post" action="allresolved.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">all resolved</button>
			</form>
		</li>
		<li>
			<form class="form-inline"  method="post" action="allemp.do">
				<button class="btn btn-primary my-2 my-sm-0 active" type="submit">all Employees</button>
			</form>
		</li>

	</ul>
	<ul class="nav navbar-nav navbar-right">
		<li>
			<form class="form-inline"  method="post" action="logout.do">
				<button class="btn btn-primary my-2 my-sm-0" type="submit">Log Out</button>
			</form>
		</li>
	</ul>
	</nav>

	<form>
		<input id="fname" type="text" placeholder="First Name" value="" /> 
		<input id="lname" type="text" placeholder="Last Name" /> 
		<input id="username" type="text" placeholder="Username" /> 
		<!-- <input id="password" type="text" placeholder="Password" />  -->
		<input id="phone" type="text" placeholder="Phone Number" /> 
		<input id="email" type="text" placeholder="Email" />

		<input id="updateInfo" type="button" value="Submit"/>
		<input id="clear" type="button" value="Clear"/>
	</form> S
</body>
</html>